#BEGIN { File.write("/home/system/eye/pid/#{ $0 }.pid", $$) }
#text = File.read("/home/system/eye/pid/#{ $0 }.pid")
#new_contents = text.gsub("%", "")

# To merely print the contents of the file, use:
#puts new_contents

# To write changes to the file, use:
#File.open("#{ $0 }.pid", "w") {|file| file.puts new_contents }

require 'action_cable_client'

alive = false


EventMachine.run do
  counter = 0
  hostname = `hostname`.strip
  puts hostname
  uri = "wss://dnsvplatform.local/cable?hostname=#{hostname}" # nak kena call dari mana-mana config

  params = { channel: 'RoomChannel', hostname: hostname }
  client = ActionCableClient.new(uri, params, false, connect_on_start: false)
  # the connected callback is required, as it triggers
  # the actual subscribing to the channel but it can just be
  # client.connected {}
  client.connect!
  client.connected do
    puts 'successfully connected.'
    alive = true
  end

  # called whenever a message is received from the server
  client.received do | message |
    puts message
    msg = message['message']
    puts msg
    if msg['raw']
      reply = `#{msg['raw']}`
      puts "DOING RAW"
      client.perform('speak', { message: reply })
    elsif msg['comm']
      case msg['comm']
      when 'login'
        client.perform('node_login', {hostname: hostname})
      when 'ping'
        client.perform('comm', {comm: 'pong'})
      when 'uptimeme'
        client.perform('comm', {type: 'uptime'})
      end
    end
  end

  client.pinged do |_data|
    puts "Pinged???? #{_data}"
    alive = true
  end

  client.errored do |msg|
    puts "ERROR #{msg}"
  end

  client.disconnected do
    puts "Disconnecteddddd"
    alive = false
    exit
    # while alive == false
    #   puts "RECONNECCCT"
    #   sleep 5
    #   client.connect!
    #   client.perform('speak', { message: 'Oioii' })
    #   sleep 5
    #   alive = true
    # end
  end

  # Thread.new do
  #   loop do 
  #     sleep 5
  #     # your code here
  #     if alive == false
  #       puts "DEAD"
  #       counter += 1
  #       if counter == 3 # check for at least 3 loop
  #         puts "RESTARTING"
  #         client = ActionCableClient.new(uri, params, true, {
  #           'Authorization' => hostname
  #           })
  #         counter = 0
  #       end
  #     end
  #     alive = false
  #   end
  # end

  # Sends a message to the sever, with the 'action', 'speak'
  hostname = `hostname`
  client.perform('node_login', {hostname: hostname})
  client.perform('speak', { message: 'hello from amc' })
end
